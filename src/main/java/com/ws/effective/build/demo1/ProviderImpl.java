package com.ws.effective.build.demo1;

/**
 * Created by WUSONG on 2017/12/1.
 */
public class ProviderImpl implements Provider {
    public Service newService() {
        return new ServiceImpl();
    }
}
