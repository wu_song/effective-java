package com.ws.effective.build.demo1;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by WUSONG on 2017/12/1.
 */
public class Services {

    private Services(){

    }

    private static final String DEFAULT_PROVIDER_NAME = "DEFAULT";

    private static final Map<String,Provider> providers = new ConcurrentHashMap<String, Provider>();

    public static void registerDefaultProvider(Provider provider){
        providers.put(DEFAULT_PROVIDER_NAME,provider);
    }

    public static void registerProvider(String name, Provider provider){
        providers.put(name, provider);
    }

    public static Service newInstance(){
        return newInstance(DEFAULT_PROVIDER_NAME);
    }

    public static Service newInstance(String name){
        Provider provider = providers.get(name);
        if (provider == null){
            throw new IllegalArgumentException("No provider registerd with name :" + name);
        }
        return provider.newService();
    }
}
