package com.ws.effective;

import com.ws.effective.build.demo2.NutritionFacts;
import org.junit.Test;

/**
 * Created by WUSONG on 2017/12/1.
 */
public class TestBuilder {

    @Test
    public void testBuilder(){
        NutritionFacts cocaCola = new NutritionFacts.Builder(240, 80)
        .calories(100).sodium(35).carbohydrate(27).build();
        System.out.println(cocaCola);
    }
}
